const fs = require('fs');
const yaml = require('js-yaml');

try {
    let fileContents = fs.readFileSync('./api/config/environment.yaml', 'utf8');
    let data = yaml.load(fileContents);

    let roit_manager = data.roit_manager
    if (roit_manager.sandbox == null) {
        roit_manager["sandbox"] = roit_manager.hom;
    }
    
    let yamlStr = yaml.dump(roit_manager, { noRefs: true });
    fs.writeFileSync('./api/env.yaml', yamlStr, 'utf8');
    
} catch (e) {
    console.log(e);
}