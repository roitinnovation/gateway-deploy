library 'devops-shared-libraries'
void awsAuthenticate(){
    withCredentials([
        string(credentialsId: 'aws_access_key_id', variable: 'aws_access_key_id'),
        string(credentialsId: 'aws_secret_access_key', variable: 'aws_secret_access_key')
    ]){   
    sh 'aws configure set aws_access_key_id ${aws_access_key_id} && aws configure set aws_secret_access_key ${aws_secret_access_key} && aws configure set preview.cloudfront true'
    }
}


pipeline {
    agent {

        kubernetes {
            inheritFrom 'jenkins-cloud-nodejs12'
            defaultContainer 'jenkins-cloud-nodejs12'
        }

     }
    environment {
        NAMEAPP = "${env.NAMEAPP}"
        TAG = "${env.TAG}"
        MODE = "${env.MODE_DEPLOY}"
        REPOSITORY = "${env.REPOSITORY}"
        BRANCH = "${env.BRANCH.replace('origin/', '').trim()}"
        WORKSPACE = "${env.WORKSPACE}"
    }
    stages {
     stage('Check Product in Test') { 
         steps {
            checkSolutionTest()
         }
     }
        stage('Clone') {
            steps {
                dir('gateway-integration') {
                    git(url: 'git@bitbucket.org:roitinnovation/roit-apidocs-awsapigateway-integration.git', credentialsId: 'SSH_BITBUCKET', branch: "master")
                    sh "ls -l"
                }
                dir('api'){
                    echo "Cloning repository: ${REPOSITORY}"
                    script{
                        if (BRANCH.startsWith("v")) {
                            echo "TAG ${BRANCH}"
                            checkout([$class: 'GitSCM', 
                                branches: [[name: "refs/tags/${BRANCH}"]], 
                                userRemoteConfigs: [[
                                    credentialsId: 'SSH_BITBUCKET', 
                                    refspec: '+refs/tags/*:refs/remotes/origin/tags/*', 
                                    url: '${REPOSITORY}']]
                            ])
                        }
                        else {
                            echo "BRANCH ${BRANCH}"
                            checkout([$class: 'GitSCM', 
                                branches: [[name: "*/${BRANCH}"]], 
                                doGenerateSubmoduleConfigurations: false,
                                userRemoteConfigs: [[
                                    credentialsId: 'SSH_BITBUCKET', 
                                    url: '${REPOSITORY}']]
                            ])
                        }
                        sh "ls -l"
                    }
                }
            }
        }

        stage('Build') {
            steps {
                echo "Starting the process with following parameters: project ${NAMEAPP} on environment ${MODE}..."
            }
        }

        stage('Test') {
            steps {
                echo 'Nothing to test ...'
            }
        }

        stage('Deploy') {
            steps {
                dir('gateway-integration'){
                    sh "node -v"
                    sh "npm i"
                    sh "npm run build"
                }
                sh "cat run.js && npm i --save js-yaml && node run.js && cat ./api/env.yaml"
                 script{
                    if(!fileExists("$env.WORKSPACE/api/src")){
                        sh "mkdir ./api/src && mv ./api/config_apidoc/* ./api/src/"
                    }
                    sh "mkdir ./api/template && cp -R ./gateway-integration/template ./api/apidoc/"
                    
                }
                dir('api'){
                    awsAuthenticate()
                    sh '''
                        #!/bin/bash

                        if [ "${MODE}" = "dev" ] || [ "${MODE}" = "sandbox" ]
                        then
                            MODE_SELECT="sandbox"
                            NODE_MODE="DeploySandbox"
                        elif [ "${MODE}" = 'hom' ]
                        then
                            MODE_SELECT="hom"
                            NODE_MODE="DeployHomologation"
                        elif [ "${MODE}" = 'prod' ]
                        then
                            MODE_SELECT="prod"
                            NODE_MODE="DeployProduction"
                        fi
                        echo $MODE_SELECT
                        echo $NODE_MODE
                        echo $WORKSPACE
                        ENV=${MODE_SELECT} OVERRIDE_EXISTING_APIS=${OVERRIDE_EXISTING_APIS} NO_ALLOW_SERVICE_UNAUTHENTICATED=${NO_ALLOW_SERVICE_UNAUTHENTICATED} PROPERTY=firestoreCredentialPrincipal{5}:roit-api-gateway-deployer-credential.json node ${WORKSPACE}/gateway-integration/dist/deployers/${NODE_MODE}.js
                        ls -l
                        SERVICE_NAME=$(find . -name "*-swagger.json" | sed "s/-swagger.json//" | sed "s/.\\///")
                        aws s3 --region sa-east-1 sync ./apidoc/${SERVICE_NAME} s3://roit-apisdoc/${MODE_SELECT}/doc/${SERVICE_NAME}/ --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers
                        aws s3 --region sa-east-1 cp ./${SERVICE_NAME}-swagger.json s3://roit-apisdoc/${MODE_SELECT}/swagger-schemes/${SERVICE_NAME}-swagger.json --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers
                        aws s3 --region sa-east-1 cp ./resource-insomnia-${SERVICE_NAME}.json s3://roit-apisdoc/${MODE}/insomnia/resource-insomnia-${SERVICE_NAME}.json --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers
                        aws s3 --region sa-east-1 cp ./resource-insomnia-ALL.json s3://roit-apisdoc/${MODE}/insomnia/resource-insomnia-ALL.json --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers
                        aws s3 --region sa-east-1 cp ./resource-postman-${SERVICE_NAME}.json s3://roit-apisdoc/${MODE_SELECT}/postman/resource-postman-${SERVICE_NAME}.json --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers
                        aws s3 --region sa-east-1 cp ./resource-postman-ALL.json s3://roit-apisdoc/${MODE_SELECT}/postman/resource-postman-ALL.json --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers
                    '''
                }
            }
        }
    }
        post {
            always {
                cleanWs()
            }
        }
}