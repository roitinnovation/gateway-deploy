
## [v1.2.5](https://bitbucket.org/roitinnovation/gateway-deploy/compare/v1.2.5..v1.2.4)

2022-12-29

### Docs

* **changelog:** generated CHANGELOG.md file

### Fix

* **service-unauthenticated:** add to set no_allow_service_unautheticated

### Pull Requests

* Merged in hotfix/RBANK-9761 (pull request [#14](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/14/))


## [v1.2.4](https://bitbucket.org/roitinnovation/gateway-deploy/compare/v1.2.4..v1.2.3)

2022-08-11

### Docs

* **changelog:** generated CHANGELOG.md file

### Fix

* **branch-name:** updated quotes used in reference to variable 'BRANCH'

### Pull Requests

* Merged in hotfix/INF-541 (pull request [#13](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/13/))


## [v1.2.3](https://bitbucket.org/roitinnovation/gateway-deploy/compare/v1.2.3..v1.2.2)

2022-08-11

### Docs

* **changelog:** generated CHANGELOG.md file

### Fix

* **branch-name:** added method 'trim()' to remove blank space in branch name

### Pull Requests

* Merged in hotfix/INF-541 (pull request [#12](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/12/))


## [v1.2.2](https://bitbucket.org/roitinnovation/gateway-deploy/compare/v1.2.2..v1.2.1)

2022-08-11

### Fix

* **branch-name:** added function to replace name branch if contains 'origin/'

### Pull Requests

* Merged in hotfix/INF-541 (pull request [#11](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/11/))


## [v1.2.1](https://bitbucket.org/roitinnovation/gateway-deploy/compare/v1.2.1..v1.2.0)

2022-07-20

### Docs

* **changelog:** generated CHANGELOG.md file


## [v1.2.0](https://bitbucket.org/roitinnovation/gateway-deploy/compare/v1.2.0..v0.0.0.0)

2022-06-30

### Feat

* **gateway-deploy:** added step to verify the existence of the src folder in stage deploy

### Pull Requests

* Merged in develop (pull request [#10](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/10/))
* Merged in feature/INF-470 (pull request [#9](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/9/))


## [v0.0.0.0](https://bitbucket.org/roitinnovation/gateway-deploy/compare/v0.0.0.0..v1.1.1)

2022-06-28

### Chore

* **change-jenkinsfile:** added stage Check Product in Test in jenksfile in jenksfile

### Docs

* **changelog:** generated CHANGELOG.md file

### Feat

* **jeankins:** update deploy step

### Fix

* **jeankins:** update deploy step to verify path


## [v1.1.1](https://bitbucket.org/roitinnovation/gateway-deploy/compare/v1.1.1..v1.1.0)

2021-12-20

### Fix

* **branch-validation:** check if starts with v

### Pull Requests

* Merged in hotfix/MGIIA-1125 (pull request [#8](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/8/))


## [v1.1.0](https://bitbucket.org/roitinnovation/gateway-deploy/compare/v1.1.0..v1.0.3)

2021-12-20

### Feat

* **branch-deploy:** added strategy to deploy branch on roit manager

### Pull Requests

* Merged in develop (pull request [#7](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/7/))
* Merged in feature/SGRA-34 (pull request [#6](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/6/))


## [v1.0.3](https://bitbucket.org/roitinnovation/gateway-deploy/compare/v1.0.3..v1.0.2)

2021-10-20

### Docs

* **changelog:** generated CHANGELOG.md file

### Fix

* **agent:** changed agent to nodejs12
* **credential:** added correct credential id
* **credential:** change credentials


## [v1.0.2](https://bitbucket.org/roitinnovation/gateway-deploy/compare/v1.0.2..v1.0.1)

2021-10-19

### Chore

* **clone-repository:** force agent jenkins-slave-nodejs10 on deploy
* **clone-repository:** changed awsAuthenticate with correct code

### Docs

* **changelog:** generated CHANGELOG.md file

### Fix

* **clone-repository:** changed credetialsId with new parameterr on project target clone
* **clone-repository:** changed credetialsId with new parameter and remove hardcode AWS credentials

### Pull Requests

* Merged in hotfix/MGIIA-866 (pull request [#5](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/5/))


## [v1.0.1](https://bitbucket.org/roitinnovation/gateway-deploy/compare/v1.0.1..v1.0.0)

2021-09-09

### Docs

* **changelog:** generated CHANGELOG.md file

### Fix

* **build-gateway:** remove unused commands to build direct from master dependencies

### Pull Requests

* Merged in hotfix/MGIIA-694 (pull request [#4](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/4/))


## v1.0.0

2021-06-21

### Feat

* **jenkinsfile:** changed function to autenticate on AWS hardcode
* **jenkinsfile:** changed function name from awsAuthenticate to roitAwsAuthenticate
* **jenkinsfile:** changed approach to autenticate AWS hardcore until check AWS autentication on cloud bee
* **jenkinsfile:** changed parameter AWS autentication pipeline on jenkinsfile
* **jenkinsfile:** changed AWS autentication pipeline on jenkinsfile
* **jenkinsfile:** added AWS credential on pipeline
* **jenkinsfile:** create pipeline to publish services on ROIT MANAGER by jenkins pipeline

### Pull Requests

* Merged in develop (pull request [#3](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/3/))
* Merged in feature/MGIIA-227 (pull request [#1](https://bitbucket.org/roitinnovation/gateway-deploy/pull-requests/1/))

